//
//  CustomerListCell.swift
//  CITIRMApp
//
//  Created by Yokeshwaran on 7/18/17.
//  Copyright © 2017 Rampalverma. All rights reserved.
//

import UIKit

class CustomerListCell: UITableViewCell {
    
    @IBOutlet var selectedBtn : UIButton!
    @IBOutlet var avatarLabel : UILabel!
    @IBOutlet var nameLabel : UILabel!
    @IBOutlet var productLabel : UILabel!
    @IBOutlet var customerIdLabel : UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        avatarLabel.layer.cornerRadius = avatarLabel.frame.width / 2
        avatarLabel.layer.masksToBounds = true
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
