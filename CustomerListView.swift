//
//  CustomerListView.swift
//  CITIRMApp
//
//  Created by Yokeshwaran on 7/18/17.
//  Copyright © 2017 Rampalverma. All rights reserved.
//

import UIKit

class CustomerListView: UIView {
    
    @IBOutlet var customerListTableView : UITableView!
    

    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */

}

extension CustomerListView : UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 5
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "CustomerListCell") as! CustomerListCell
        
       
        
        return cell
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 70
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
}

extension CustomerListView : UITableViewDelegate {
    
    
    
}
