//
//  Constants.swift
//  CitiControl
//
//  Created by Madhav Bhogapurapu on 12/06/17.
//  Copyright © 2017 VirtusaPolaris. All rights reserved.
//

import Foundation
import UIKit

//let primaryBGColor = UIColor(hex: "#ECECE9")
let blueTitleBarColor = UIColor(hex: "#3779B1")
let lightBlueColor = UIColor(hex: "#5BB6E7")
let darkGrayColor = UIColor(hex: "#3F4B60")

let menuGrayColor = UIColor(hex: "#EDEDEA")

let  primaryBGColor = UIColor(hex: "#EFEFEE")
let peachRedColor = UIColor(hex: "#EB756E")

struct ChartColors {
    
    static var redColor: UIColor  { return UIColor(hex: "#e54343") }
    static var amber: UIColor  { return UIColor(hex: "#ffcd00") }
    static var amberDark: UIColor  { return UIColor(hex: "#e98903") }
    static var greeen: UIColor  { return UIColor(hex: "#42bc12") }
    static var lightBlue: UIColor  { return UIColor(hex: "#21b7eb") }
    static var greeenLight: UIColor  { return UIColor(hex: "#bfd130") }
    static var purpleColor: UIColor  { return UIColor(hex: "#c4007f") }

    static var gray: UIColor  { return UIColor(hex: "#999999") }

    
}
