//
//  JSONHelper.swift
//  CitiControl
//
//  Created by Madhav Bhogapurapu on 15/06/17.
//  Copyright © 2017 VirtusaPolaris. All rights reserved.
//

import Foundation
class JSONHelper
{
    //static let sharedInstance = JSONHelper()
    
    class func getJSONData(fileName: String) -> [String : Any]?
    {
        let filePath = Bundle.main.path(forResource: fileName, ofType: "json")
        
        let jsonData = try? Data(contentsOf: URL(fileURLWithPath: filePath!), options: .alwaysMapped)
        
        do {
            let jsonResult = try JSONSerialization.jsonObject(with: jsonData! as Data, options: .allowFragments) as! [String : Any]

        
            return jsonResult
            
        } catch {
            
        print(error)
        }
        
        return nil
    }
    
}
