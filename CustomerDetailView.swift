//
//  CustomerDetailView.swift
//  CITIRMApp
//
//  Created by Yokeshwaran on 7/18/17.
//  Copyright © 2017 Rampalverma. All rights reserved.
//

import UIKit
import MapKit

class CustomerDetailView: UIView {

    @IBOutlet var customerLocationView : MKMapView!
    @IBOutlet var avatarLabel : UILabel!
    @IBOutlet var nameLabel : UILabel!
    @IBOutlet var customerIdLabel : UILabel!
    @IBOutlet var addressLabel : UILabel!
    
    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */

}

extension CustomerDetailView : MKMapViewDelegate {
    
    
}
