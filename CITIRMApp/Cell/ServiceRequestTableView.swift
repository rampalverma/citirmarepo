//
//  ServiceRequestTableView.swift
//  CITIRMApp
//
//  Created by rampal verma on 19/07/17.
//  Copyright © 2017 Rampalverma. All rights reserved.
//

import UIKit

class ServiceRequestTableView: CustomTableView {

   override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int{
        
        return dataArray.count
        
        //return 0
    }
    
   override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell{
        let cellIdentifier = getCellIdentifier()
        
        let cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier, for: indexPath) as! DashboardCellTableViewCell
        
        
        setupCell(cell: cell, indexPath: indexPath)
        
        
        return cell
    }
    
   override func setupCell(cell : DashboardCellTableViewCell, indexPath: IndexPath)
    {
        let dict = dataArray[indexPath.row]
        
       
            cell.nameLabel.text = dict["name"] as? String
            cell.acntNumberLabel.text = dict["accountNumber"] as? String
            cell.acccountTypeLabel.text = dict["accountType"] as? String
            cell.timeLabel.text = dict["dateTime"] as? String
            cell.requestNumberLabel.text = dict["requestId"] as? String
            //        if self.tag == 2
            //        {
            cell.progressView.titleText = dict["status"] as? String
            cell.progressView.setNeedsDisplay()
        //  }
        
    }

    override func reloadTableView(contentArray: [[String : Any]]?) {
        
        if let dataArray = contentArray
        {
            self.delegate = self
            self.dataSource = self
           self.dataArray = dataArray
        self.dataArray.removeFirst()
            
            self.reloadData()
        }
        
    }

}
