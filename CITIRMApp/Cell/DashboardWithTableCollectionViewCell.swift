//
//  DashboardWithTableCollectionViewCell.swift
//  CITIRMApp
//
//  Created by Harshita on 14/07/17.
//  Copyright © 2017 Rampalverma. All rights reserved.
//

import UIKit

class DashboardWithTableCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var cellTableView: CustomTableView!
    
    @IBOutlet weak var paymentTableView: UpComingsTableView!
   
    @IBOutlet weak var requestTableView: ServiceRequestTableView!
    
    @IBOutlet weak var performanceTableView: PerformanceTableView!
    @IBOutlet weak var profitabilityTableView: ProfitabilityTableView!
    @IBOutlet weak var portfolioTableView: PortfolioTableView!
    
}
