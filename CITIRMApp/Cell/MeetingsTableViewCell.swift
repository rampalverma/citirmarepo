//
//  MeetingsTableViewCell.swift
//  CitiIndia
//
//  Created by admin on 10/07/17.
//  Copyright © 2017 admin. All rights reserved.
//

import UIKit

class MeetingsTableViewCell: UITableViewCell {
    @IBOutlet var lblSubject:UILabel!
    @IBOutlet var lblCreatedBy:UILabel!
    @IBOutlet var lblStatus:UILabel!
    @IBOutlet var lblTime:UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
