//
//  DashboardCellTableViewCell.swift
//  CITIRMApp
//
//  Created by Harshita on 14/07/17.
//  Copyright © 2017 Rampalverma. All rights reserved.
//

import UIKit

class DashboardCellTableViewCell: UITableViewCell {

    @IBOutlet weak var progressView: ProgressView!
    
    @IBOutlet weak var acccountTypeLabel: UILabel!
    
    @IBOutlet weak var timeLabel: UILabel!
    @IBOutlet weak var descLabel: UILabel!
    @IBOutlet weak var acntNumberLabel: UILabel!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var codeLabel: UILabel!
    @IBOutlet weak var amountLabel: UILabel!

    @IBOutlet weak var requestNumberLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
    }
    
    func makeRoundedLabel(text : String?)
    {
        codeLabel.text = getFirstLettersOfTitle(text: text!)
        
        codeLabel.layer.cornerRadius = codeLabel.frame.height/2
        codeLabel.layer.masksToBounds = true
        codeLabel.backgroundColor = getRandomColor()
        codeLabel.font = UIFont.arialRegular(size: 12)
    }
    
    func getRandomColor() -> UIColor{
        
        let randomRed:CGFloat = CGFloat(drand48())
        
        let randomGreen:CGFloat = CGFloat(drand48())
        
        let randomBlue:CGFloat = CGFloat(drand48())
        
        return UIColor(red: randomRed, green: randomGreen, blue: randomBlue, alpha: 1.0)
        
    }
    
    func getFirstLettersOfTitle(text: String) -> String{
        let textStr = text.components(separatedBy: " ")
        var requiredText = ""
        for str in textStr {
            requiredText = requiredText + String(str.characters.first!)
            if requiredText.characters.count == 2 {
                break
            }
        }
        return requiredText
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
