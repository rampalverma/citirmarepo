//
//  UpComingsTableView.swift
//  CITIRMApp
//
//  Created by rampal verma on 19/07/17.
//  Copyright © 2017 Rampalverma. All rights reserved.
//

import UIKit

class UpComingsTableView: CustomTableView {

  override  func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int{
        
        return dataArray.count
        //return 0
    }
    
   override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell{
        let cellIdentifier = getCellIdentifier()
        
        let cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier, for: indexPath) as! DashboardCellTableViewCell
        
        
        setupCell(cell: cell, indexPath: indexPath)
        
        
        return cell
    }
    
  override  func setupCell(cell : DashboardCellTableViewCell, indexPath: IndexPath)
    {
        let dict = dataArray[indexPath.row]
       
            
        cell.nameLabel.text = dict["name"] as? String
        cell.acntNumberLabel.text = dict["accountNumber"] as? String
        if let amount = dict["amount"] as? Double
        {
            cell.amountLabel.text = NSNumber.formattedCurrency(price: amount)
        }
        //cell.descLabel.text = dict["description"] as? String
        
        if let dateStr = dict["dueTime"] as? String
        {
            let shortDate = Date.dateToString(date: Date.stringToDate(stringDate: dateStr, withFormat: "MMM dd yyyy"), withFormat: "MMM dd")
            cell.timeLabel.text = shortDate
            
        }
        cell.makeRoundedLabel(text: cell.nameLabel.text)
            
       }


}
