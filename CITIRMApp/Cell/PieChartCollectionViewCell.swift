//
//  PieChartCollectionViewCell.swift
//  CITIRMApp
//
//  Created by rampal verma on 17/07/17.
//  Copyright © 2017 Rampalverma. All rights reserved.
//

import UIKit
import Charts

class PieChartCollectionViewCell: UICollectionViewCell,IValueFormatter {
    @IBOutlet weak var chartView: PieChartView!
    let sectorArray = [35.0,20.0,20.0,15,10]
    
    override func awakeFromNib() {
        super.awakeFromNib()
        //self.setShadow()
        // Initialization code
    }
    
    func setupPieChartView(indexPath : IndexPath)
    {
        let l = chartView.legend
        
        l.horizontalAlignment = .right
        l.verticalAlignment = .top
        l.orientation = .vertical
        l.drawInside = false
        
        l.form = .square
        
        //l.formSize = 4.0;
        l.font = UIFont.arialRegular(size: 9);
        l.xEntrySpace = 4.0;
        l.yEntrySpace = 9.0;
        chartView.chartDescription?.enabled = false
        
        chartView.extraRightOffset = 100
        chartView.extraTopOffset = 2
        chartView.extraLeftOffset = -180
        //chartView.extraRightOffset = 0.0
        chartView.extraBottomOffset = 2
        
        updateBarChartData(count: 5, range: 20)
    }
    
    func updateBarChartData(count : Int, range: Double)
        
    {
        //let  spaceForBar = 2
        var valuesArray = [PieChartDataEntry]()
        //var xVals = [BubbleChartDataEntry]()
        //let size =  10.0
        
        let titlesArray = ["CITIGOLD", "CITI BUSINESS", "CITIGOLD BUSINESS", "CITY PRIORITY", "OTHERS"]
        
        //let unitsY = [30.0, 34.0, 25.0, 20.0, 28.0]
        
        for index in 0..<sectorArray.count
        {
            //let val =  arc4random_uniform(UInt32(range))
            //let xval =  arc4random_uniform(UInt32(range))
            
            //let size =  Double(arc4random_uniform(UInt32(range)))
            
            let dataEntry = PieChartDataEntry(value: sectorArray[index], label: titlesArray[index]  , data: nil)
            //  dataEntry.size = 35
            valuesArray.append(dataEntry)
            
        }
        
        let chartDataSet = PieChartDataSet(values: valuesArray, label: "")
        chartDataSet.colors = [ChartColors.purpleColor,ChartColors.amber,ChartColors.greeenLight, ChartColors.amberDark, ChartColors.lightBlue,ChartColors.greeen]
        
        chartDataSet.drawValuesEnabled = true
        chartDataSet.highlightEnabled = true
        chartDataSet.automaticallyDisableSliceSpacing = true
        chartDataSet.selectionShift = 8.0
        chartDataSet.sliceSpace = 1.0
        chartDataSet.formLineWidth = 30
        
        // chartDataSet.iconsOffset = CGPointMake(0, 40);
        
        let dataSets: [PieChartDataSet] = [chartDataSet]
        
        let chartData = PieChartData(dataSets: dataSets)
        
        chartData.setValueFont(UIFont.arialRegular(size: 6))
        
        let format = NumberFormatter()
        format.numberStyle = .percent
        format.generatesDecimalNumbers = false
        format.usesSignificantDigits = true
        format.maximumFractionDigits = 1
        format.multiplier = Float(1.0) as NSNumber?
        format.percentSymbol = "%"
        
        
        chartData.setValueFormatter(DefaultValueFormatter(formatter: format))
        
        chartView.data = chartData
        // chartView.sizeToFit()
        //chartView.setVisibleXRangeMaximum(200)
        
        // dataEntries.append(dataEntry)
        
        
    }
    
    func stringForValue(_ value: Double,
                        entry: ChartDataEntry,
                        dataSetIndex: Int,
                        viewPortHandler: ViewPortHandler?) -> String
    {
        return ""
    }
    
    
}
