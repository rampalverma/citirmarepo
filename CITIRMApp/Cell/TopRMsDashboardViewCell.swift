//
//  TopRMsDashboardViewCell.swift
//  CITIRMApp
//
//  Created by Harshita on 22/07/17.
//  Copyright © 2017 Rampalverma. All rights reserved.
//

import UIKit

class TopRMsDashboardViewCell: UICollectionViewCell {
    @IBOutlet weak var portfolioTable: UITableView!
    @IBOutlet weak var profitablityTable: UITableView!
    @IBOutlet weak var performanceTable: UITableView!
}
