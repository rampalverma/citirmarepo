//
//  MyFllowupViewController.swift
//  CitiIndia
//
//  Created by admin on 10/07/17.
//  Copyright © 2017 admin. All rights reserved.
//

import UIKit

class MyFllowupViewController: UIViewController {
    @IBOutlet weak var tblFllowup:UITableView!
    var numberOfSections = [String]()
    var allSchedules:Array<AnyObject>?
    var filterSchedules:Array<AnyObject>?
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "My Followups"
        do {
            if let file = Bundle.main.url(forResource: "allMeetings", withExtension: "json") {
                let data = try Data(contentsOf: file)
                let json = try JSONSerialization.jsonObject(with: data, options: [])
                if let object = json as? [String: Any] {
                    // json is a dictionary
                    allSchedules = object["MyFollowUps"] as? Array<AnyObject>
                    filterSchedules = object["MyFollowUps"] as? Array<AnyObject>
                    print(object)
                } else if let object = json as? [Any] {
                    // json is an array
                    print(object)
                } else {
                    print("JSON is invalid")
                }
            } else {
                print("no file")
            }
        } catch {
            print(error.localizedDescription)
        }
        groupByDate()
        tblFllowup.rowHeight = UITableViewAutomaticDimension
        tblFllowup.tableFooterView = UIView()
        // Do any additional setup after loading the view.
    }
    func groupByDate(){
        //        let dateForamt = DateFormatter()
        //        dateForamt.dateFormat = "dd/MM/yyyy"
        for object in allSchedules!{
            if !numberOfSections.contains(object.value(forKey: "dueDate") as! String){
                numberOfSections.append(object.value(forKey: "dueDate") as! String)
            }
        }
    }
    func groupedObjects(date:String)->Array<AnyObject>{
        filterSchedules = allSchedules
        let foundItems = allSchedules?.filter { (($0.value(forKey:"dueDate" ) as! String).localizedLowercase.contains(date.localizedLowercase))}
        return foundItems!
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
extension MyFllowupViewController:UITableViewDelegate,UITableViewDataSource{
    func numberOfSections(in tableView: UITableView) -> Int {
        if numberOfSections.count > 0{
            return numberOfSections.count
        }
        return 1
    }
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return numberOfSections[section]
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        let object = self.groupedObjects(date: numberOfSections[section])
        if object.count > 0
        {
            return object.count
        }
        return 0
    }
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView?
    {
        let headerView = UIView(frame: CGRect(x: 0, y: 0, width: tableView.bounds.size.width, height: 30))
        let headerLabel = UILabel(frame: CGRect(x: 5, y: 0, width: tableView.bounds.size.width, height: 30))
        headerLabel.text = numberOfSections[section]
        headerLabel.textColor = UIColor.white
        headerView.addSubview(headerLabel)
        headerView.backgroundColor = UIColor.black
        return headerView
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "MyFollowupTableViewCell") as! MyFollowupTableViewCell
        let objectsection = self.groupedObjects(date: numberOfSections[indexPath.section])
        let objectRow = objectsection[indexPath.row] as! [String:AnyObject]
        cell.lblSubject.text = objectRow["taskSubject"] as? String
        cell.lblStatus.text = objectRow["taskStatus"] as? String
        if cell.lblStatus.text == "Completed"{
            cell.lblStatus.textColor = UIColor.red
        }
        else if cell.lblStatus.text == "OverDue"{
            cell.lblStatus.textColor = UIColor.green
        }
        cell.lblAssigenbyBy.text = "Created By : \(String(describing: (objectRow["creator"]?.value(forKey: "name") as! String)))"
        cell.lblPriority.text = objectRow["xPriority"] as? String
        return cell
    }
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return 70
    }
}

