//
//  TababarViewController.swift
//  CITIRMApp
//
//  Created by Madhav Bhogapurapu on 17/07/17.
//  Copyright © 2017 Rampalverma. All rights reserved.
//

import UIKit

class TababarViewController: UITabBarController {

    var listViewControllers : [UIViewController]!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        let value = UIInterfaceOrientation.landscapeLeft.rawValue
        UIDevice.current.setValue(value, forKey: "orientation")
        // Do any additional setup after loading the view.
    }

    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        listViewControllers = self.viewControllers
        
    }
    
    func setSelectedTabIndex(index: Int)
    {
        self.selectedViewController = listViewControllers[index]
        
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    private func supportedInterfaceOrientations() -> UIInterfaceOrientationMask {
        return UIInterfaceOrientationMask.landscapeLeft
    }
    private func shouldAutorotate() -> Bool {
        return true
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
