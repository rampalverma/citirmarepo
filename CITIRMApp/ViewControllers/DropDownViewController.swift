//
//  DropDownViewController.swift
//  CITIRMApp
//
//  Created by admin on 14/07/17.
//  Copyright © 2017 Rampalverma. All rights reserved.
//

import UIKit
protocol DropDownDelegate {
    func didSelectDropDown(selectedObject:String,selectedIndex:Int)
}
class DropDownViewController: BaseViewController {
    @IBOutlet var tableview:UITableView!
    @IBOutlet var dataePicker:UIDatePicker!
    var tableArray:Array<String>?
    var delegate:DropDownDelegate?
    override func viewDidLoad() {
        super.viewDidLoad()
  
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
extension DropDownViewController:UITableViewDelegate,UITableViewDataSource{
    
    func numberOfSections(in tableView: UITableView) -> Int {
        if let _ = tableArray{
            return 1
        }
        return 0
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return (tableArray?.count)!
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableview.dequeueReusableCell(withIdentifier:"DropDownTableViewCell" ) as! DropDownTableViewCell
        cell.lblValue.text = tableArray?[indexPath.row]
        return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        delegate?.didSelectDropDown(selectedObject: (tableArray?[indexPath.row])!, selectedIndex: indexPath.row)
        self.navigationController?.popViewController(animated: true)
    }
}
