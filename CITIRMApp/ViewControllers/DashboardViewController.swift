//
//  DashboardViewController.swift
//  CITIRMApp
//
//  Created by Harshita on 14/07/17.
//  Copyright © 2017 Rampalverma. All rights reserved.
//

import UIKit

class DashboardViewController: BaseViewController, UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout, CustomtTableViewDelegate {

    var dataDict = [String : Any]()
    var sectionTitlesArray = [String]()
    
    @IBOutlet weak var dashCollectionView: UICollectionView!
    
    @IBOutlet weak var leftMenuView: UIView!
    
    func getJSONData()
    {
        guard let dict =  JSONHelper.getJSONData(fileName: "Dashboard") else {
            return
        }
        
        dataDict = dict["data"] as! [String : Any]
        sectionTitlesArray.append(contentsOf: dataDict.keys)
        
        dashCollectionView.reloadData()
        
    }
    @IBAction func historyMenuActions(_ sender: UIButton) {
        switch sender.tag {
        case 1: print("button1")
        case 2: print("button2")
        case 3: print("button2")
            
        default:
            break
        }
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        getJSONData()
        
       // createLeftMenuOptions()
        // Do any additional setup after loading the view.
    }

    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return sectionTitlesArray.count
    }
    
    func getCellIndentifier(indexPath: IndexPath) ->String
    {
        var cellIdentifier = "DashboardWithTableCollectionViewCell"
        
        switch indexPath.item {
        case 0 : cellIdentifier = "DashboardWithTableCollectionViewCell"
        
        case 2: cellIdentifier = "DashboardPaymentsCell"
        case 1 : cellIdentifier = "PieChartCollectionViewCell"
        case 3: cellIdentifier = "ServiceRequestCell"
  
        default: break
        }
        
        return cellIdentifier
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cellIdentifier = getCellIndentifier(indexPath: indexPath)
        var sectionTitle = sectionTitlesArray[indexPath.section] as String
        
        switch indexPath.item {
        case 0: let cell = collectionView.dequeueReusableCell(withReuseIdentifier:cellIdentifier, for: indexPath) as! DashboardWithTableCollectionViewCell
        cell.cellTableView.customDelegate = self
        cell.cellTableView.reloadTableView(contentArray:self.dataDict[sectionTitle] as? [[String : Any]] )
        return cell

        case 1:
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier:cellIdentifier, for: indexPath) as! PieChartCollectionViewCell
        DispatchQueue.main.async {
            cell.setupPieChartView(indexPath: indexPath)
        }
        return cell

        case 2:
        sectionTitle =  "Upcoming Payments"
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier:cellIdentifier, for: indexPath) as! DashboardWithTableCollectionViewCell
        cell.cellTableView.customDelegate = self
        cell.paymentTableView.reloadTableView(contentArray:self.dataDict[sectionTitle] as? [[String : Any]] )
        return cell

        case 3: let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "ServiceRequestCell", for: indexPath) as! DashboardWithTableCollectionViewCell
        DispatchQueue.main.async {
        cell.requestTableView.reloadTableView(contentArray:self.dataDict[sectionTitle] as? [[String : Any]] )
        }
        
        return cell

        default:
            break
    
        }
//
       
        return UICollectionViewCell()
        
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        var itemsize = CGSize()
        switch indexPath.row {
        case 0,2:
            itemsize = CGSize(width: 540, height: 240)
            //itemsize = CGSize(width: 512, height: 275)
        case 1,3:
            itemsize = CGSize(width: 334, height: 240)
            //itemsize = CGSize(width: 340, height: 275)

        default:
            break
        }
        return itemsize
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets(top: 15, left: 20, bottom: 15, right: 20)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 10
    }
    
    
    func didSelectRow(indexPath: IndexPath, table: CustomTableView) {
        if table.tag == 1
        {
            
        }
        
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
