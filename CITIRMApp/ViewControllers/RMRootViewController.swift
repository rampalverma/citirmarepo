//
//  RMRootViewController.swift
//  CITIRMApp
//
//  Created by rampal verma on 22/07/17.
//  Copyright © 2017 Rampalverma. All rights reserved.
//

import UIKit

class RMRootViewController: BaseViewController {

    @IBOutlet weak var leftMenuView: UIView!
    @IBOutlet weak var containerView: UIView!
    var tabbarVC : TababarViewController?
    
    @IBOutlet weak var profileImageView: UIImageView!
    
    @IBOutlet weak var userNameLabel: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        createLeftMenuOptions()
        // Do any additional setup after loading the view.
    }
    
    @IBAction func historyMenuActions(_ sender: UIButton) {
        switch sender.tag {
        case 1: print("button1")
        case 2: print("button2")
        case 3: print("button2")
            
        default:
            break
        }
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func createLeftMenuOptions()
    {
        profileImageView.drawCircle()
        
        let titlesArray = ["Dashboard", "RMList", "Calendar", "Inbox", "Settings"]
        
        let topY =  userNameLabel.frame.origin.y + userNameLabel.frame.size.height + 35
        
        for index in 0..<titlesArray.count{
            let menuItem = CustomViews.fromNib("CustomViews", tag: 101)
            var rect = menuItem.frame
            rect.origin.y = topY + rect.size.height*CGFloat(index)
            menuItem.frame = rect
            menuItem.tag = index
            if index != 0
            {
                menuItem.borderView.backgroundColor = .clear
            }
            menuItem.titleLabel.text = titlesArray[index]
            leftMenuView.addSubview(menuItem)
            menuItem.borderView.tag = index
            
            let tapGesture = UITapGestureRecognizer(target: self
                , action: #selector(menuTapAction(recognizer:)))
            menuItem.addGestureRecognizer(tapGesture)
            
            
        }
    }
    
    func setHighlightMenuItem(itemView : CustomViews)
    {
        for (_,menuItemView) in (leftMenuView.subviews.filter{$0 .isKind(of: CustomViews.self)}).enumerated(){
            (menuItemView as! CustomViews).borderView.backgroundColor = (menuItemView.tag == itemView.tag) ? peachRedColor : .clear
        }
    }
    
    func menuTapAction(recognizer : UITapGestureRecognizer)
    {
        let view = recognizer.view as! CustomViews
        setHighlightMenuItem(itemView: view)
        setTabSelectedIndex(index: view.tag)
    }
    
    
    func setTabSelectedIndex(index : Int)
    {
        tabbarVC?.setSelectedTabIndex(index: index)
    }
    
    // MARK: - Navigation
    
    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if let identifier = segue.identifier,  identifier == "TabbarContainer"
        {
            tabbarVC = segue.destination as? TababarViewController
        }
        
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    
}
