//
//  MeetingsViewController.swift
//  CitiIndia
//
//  Created by admin on 10/07/17.
//  Copyright © 2017 admin. All rights reserved.
//

import UIKit
import CoreData
class MeetingsViewController: UIViewController,NSFetchedResultsControllerDelegate {
    @IBOutlet weak var tblMeetings:UITableView!
    var numberOfSections = [String]()
    var allMeetings:Array<AnyObject>?
    var filterMeetings:Array<AnyObject>?
    fileprivate lazy var fetchedResultsController: NSFetchedResultsController<Activity> = {
        // Create Fetch Request
        let fetchRequest: NSFetchRequest<Activity> = Activity.fetchRequest()
        
        // Configure Fetch Request
        fetchRequest.sortDescriptors = [NSSortDescriptor(key: "activityDate", ascending: true)]
//        fetchRequest.propertiesToGroupBy = ["dateOrTime"]
        // Create Fetched Results Controller
        let fetchedResultsController = NSFetchedResultsController(fetchRequest: fetchRequest, managedObjectContext: appdelegte.persistentContainer.viewContext, sectionNameKeyPath: "activityDate", cacheName: nil)
        
        // Configure Fetched Results Controller
        fetchedResultsController.delegate = self
      
        return fetchedResultsController
    }()
    override func viewWillAppear(_ animated: Bool) {
        tblMeetings.reloadData()
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "Meetings"
        do {
            if let file = Bundle.main.url(forResource: "allMeetings", withExtension: "json") {
                let data = try Data(contentsOf: file)
                let json = try JSONSerialization.jsonObject(with: data, options: [])
                if let object = json as? [String: Any] {
                    // json is a dictionary
                    allMeetings = object["Meetings"] as? Array<AnyObject>
                    filterMeetings = object["Meetings"] as? Array<AnyObject>
                    print(object)
                } else if let object = json as? [Any] {
                    // json is an array
                    print(object)
                } else {
                    print("JSON is invalid")
                }
            } else {
                print("no file")
            }
        } catch {
            print(error.localizedDescription)
        }
        groupByDate()
        tblMeetings.rowHeight = UITableViewAutomaticDimension
        tblMeetings.tableFooterView = UIView()
        // Do any additional setup after loading the view.
    }
    func groupByDate(){
//        let dateForamt = DateFormatter()
//        dateForamt.dateFormat = "dd/MM/yyyy"
        for object in allMeetings!{
            if !numberOfSections.contains(object.value(forKey: "scheduledDate") as! String){
               numberOfSections.append(object.value(forKey: "scheduledDate") as! String)
            }
        }
    }
   func groupedObjects(date:String)->Array<AnyObject>{
        filterMeetings = allMeetings
        let foundItems = filterMeetings?.filter { (($0.value(forKey: "scheduledDate") as! String).localizedLowercase.contains(date.localizedLowercase))}
        return foundItems!
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func loadActivity(){
        
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
extension MeetingsViewController:UITableViewDelegate,UITableViewDataSource{
    func numberOfSections(in tableView: UITableView) -> Int {
        do {
            try fetchedResultsController.performFetch()
        } catch {
            fatalError("Failed to initialize FetchedResultsController: \(error)")
        }
        guard let activities = fetchedResultsController.sections else{
            return 0
        }
        
       if activities.count > 0{
           return activities.count
       }
        return 0
    }
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        guard let activities = fetchedResultsController.sections else{
            return ""
        }
        
        return activities[section].name
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        guard let sections = fetchedResultsController.sections else {
            fatalError("No sections in fetchedResultsController")
        }
        let sectionInfo = sections[section]
        return sectionInfo.numberOfObjects
    }
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView?
    {
        let headerView = UIView(frame: CGRect(x: 0, y: 0, width: tableView.bounds.size.width, height: 30))
        let headerLabel = UILabel(frame: CGRect(x: 5, y: 0, width: tableView.bounds.size.width, height: 30))
        let activities = fetchedResultsController.sections
        headerLabel.text = activities?[section].name
        headerLabel.textColor = UIColor.white
        headerView.addSubview(headerLabel)
        headerView.backgroundColor = UIColor.black
        return headerView
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "MeetingsTableViewCell") as! MeetingsTableViewCell
       
        let activities = fetchedResultsController.object(at: indexPath)
        cell.lblStatus.text = activities.activityStatus
        if cell.lblStatus.text == "Completed"{
            cell.lblStatus.textColor = UIColor.red
        }
        else if cell.lblStatus.text == "Open"{
            cell.lblStatus.textColor = UIColor.green
        }
        cell.lblTime.text = activities.activityTime
        cell.lblCreatedBy.text = "Activity Type : \(activities.activityType ?? "")"
        cell.lblSubject.text = activities.activityDescription
        return cell
    }
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return 70
    }
}
