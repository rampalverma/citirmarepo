//
//  RMDashboardViewController.swift
//  CITIRMApp
//
//  Created by Harshita on 22/07/17.
//  Copyright © 2017 Rampalverma. All rights reserved.
//

import UIKit

class RMDashboardViewController: UIViewController, UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout, CustomtTableViewDelegate {

    var dataDict = [String : Any]()
    var sectionTitlesArray = [String]()
    
    @IBOutlet weak var dashCollectionView: UICollectionView!
    
    @IBOutlet weak var leftMenuView: UIView!
    
    func getJSONData()
    {
        guard let dict =  JSONHelper.getJSONData(fileName: "Dashboard") else {
            return
        }
        
        dataDict = dict["data"] as! [String : Any]
        sectionTitlesArray.append(contentsOf: dataDict.keys)
        
        dashCollectionView.reloadData()
        
    }
    @IBAction func historyMenuActions(_ sender: UIButton) {
        switch sender.tag {
        case 1: print("button1")
        case 2: print("button2")
        case 3: print("button2")
            
        default:
            break
        }
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        getJSONData()
        // Do any additional setup after loading the view.
    }

    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 2
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if section == 0
        {
            return 2
        }
        else
        {
        return 3
        }
    }
    
    func getCellIndentifier(indexPath: IndexPath) ->String
    {
        var cellIdentifier = "DashboardWithTableCollectionViewCell"
        
        switch indexPath.section {
        case  0:
            if indexPath.item ==  0 || indexPath.item == 1
            {
            cellIdentifier = "PieChartCollectionViewCell"
            return cellIdentifier

            }
        case 1  :
            switch indexPath.item {
            case 0:
                cellIdentifier = "PortfolioCollectionViewCell"
            case 1: cellIdentifier = "ProfitabilityCollectionViewCell"
            case 2 : cellIdentifier = "PerformanceCollectionViewCell"

            default:
                break
            }

        default: break
        }
        
        return cellIdentifier
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cellIdentifier = getCellIndentifier(indexPath: indexPath)
        var sectionTitle = sectionTitlesArray[indexPath.section] as String
        
        if indexPath.section == 0
        {
//            if indexPath.item == 0 || indexPath.item == 1
//            {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier:cellIdentifier, for: indexPath) as! PieChartCollectionViewCell
            DispatchQueue.main.async {
                cell.setupPieChartView(indexPath: indexPath)
            }
            return cell
  
            //}
        }
        else
        {
        switch indexPath.item {
        case 0: let cell = collectionView.dequeueReusableCell(withReuseIdentifier:cellIdentifier, for: indexPath) as! DashboardWithTableCollectionViewCell
        cell.portfolioTableView.customDelegate = self
        cell.portfolioTableView.reloadTableView(contentArray:self.dataDict[sectionTitle] as? [[String : Any]] )
        return cell
            
            
        case 1:
            sectionTitle =  "Upcoming Payments"
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier:cellIdentifier, for: indexPath) as! DashboardWithTableCollectionViewCell
            cell.profitabilityTableView.customDelegate = self
            cell.profitabilityTableView.reloadTableView(contentArray:self.dataDict[sectionTitle] as? [[String : Any]] )
            return cell
            
        case 2: let cell = collectionView.dequeueReusableCell(withReuseIdentifier: cellIdentifier, for: indexPath) as! DashboardWithTableCollectionViewCell
      //  DispatchQueue.main.async {
            cell.performanceTableView.reloadTableView(contentArray:self.dataDict[sectionTitle] as? [[String : Any]] )
       // }
        
        return cell
            
        default:
            break
            
        }
        }
        //
        
        return UICollectionViewCell()
        
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        var itemsize = CGSize()
//        if indexPath.section == 0
//        {
//            if indexPath.item == 0 || indexPath.item == 1
//            {
//                itemsize = CGSize(width: 435, height: 263)
//            }
//            return itemsize
//        }
//        else
//        {
        switch indexPath.section {
        case 0:
        
            itemsize = CGSize(width: 434.5, height: 263)
        //itemsize = CGSize(width: 512, height: 275)
        case 1:
            itemsize = CGSize(width: 289.66, height: 336)
            //itemsize = CGSize(width: 340, height: 275)
            
        default:
            break
        }
            
        return itemsize
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        if section == 1
        {
            return UIEdgeInsets(top: 2, left: 20, bottom: 15, right: 20)
 
        }
        return UIEdgeInsets(top: 15, left: 20, bottom: 15, right: 20)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        if section == 0
        {
            return 15
        }
        else
        {
            return 1
        }
    }
    
func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, referenceSizeForHeaderInSection section: Int) -> CGSize
    {
        let size = CGSize(width: 800, height: 48)
        if section == 0
        {
            return CGSize.zero
        }
        
        return size
    
    }
    func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {
        var reusableView = SectionHeaderCollectionReusableView()
        if indexPath.section == 1
        {
        switch kind {
            
        case UICollectionElementKindSectionHeader :
            let headerView = collectionView.dequeueReusableSupplementaryView(ofKind: kind, withReuseIdentifier: "SectionHeaderCollectionReusableView", for: indexPath) as! SectionHeaderCollectionReusableView
            var rect = headerView.frame
            rect.size.width = 884
            rect.origin.x = 20
            headerView.frame = rect
            
            reusableView = headerView
            
        default: break
            
        }
            return reusableView

        }
        return SectionHeaderCollectionReusableView()

        
    }
    
    // MARK: - Table Actions
  
    func didSelectRow(indexPath: IndexPath, table: CustomTableView) {
        if table.tag == 1
        {
            
        }
        
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
