//
//  NewActivityCellTableViewCell.swift
//  CITIRMApp
//
//  Created by Yokeshwaran on 7/14/17.
//  Copyright © 2017 Rampalverma. All rights reserved.
//

import UIKit

class NewActivityCellTableViewCell: UITableViewCell {
    
    @IBOutlet weak var label:UILabel!
    @IBOutlet weak var textField:UITextField!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
