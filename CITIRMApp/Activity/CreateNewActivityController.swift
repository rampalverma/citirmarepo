//
//  CreateNewActivityController.swift
//  CITIRMApp
//
//  Created by Yokeshwaran on 7/14/17.
//  Copyright © 2017 Rampalverma. All rights reserved.
//

import UIKit
import CoreData
class CreateNewActivityController: UIViewController {
    
    @IBOutlet weak var activityTableView:UITableView!
    var activity:Activity? = nil
    let titlesArray = [["Title":"Planned Start date/time", "icon" :""],["Title":"Name", "icon" :""],["Title":"Activity type", "icon" :""], ["Title":"Description", "icon" :""],["Title":"Customer Address", "icon" :""],["Title":"Phone number", "icon" :"", "SubTitle":"Invitees"],["Title":"Outcome", "icon" :""], ["Title":"Email address", "icon" :""]]
    @IBOutlet var datePicker:UIDatePicker!
    @IBOutlet var dateView:UIView!
    var currentTextField:UITextField?
   var isDropDownVisible:Bool = false
    override func viewDidLoad() {
        super.viewDidLoad()
        dateView.removeFromSuperview()
        if activity == nil{
            activity = NSEntityDescription.insertNewObject(forEntityName: "Activity", into: appdelegte.context()) as? Activity
        }
        self.title = "Create Activity"
        activityTableView.tableFooterView = UIView()
        // Do any additional setup after loading the view.
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        if !isDropDownVisible && activity != nil{
            appdelegte.context().delete(activity!)
            activity = nil
        }
    }
    
    @IBAction func create(){
        activity?.activityStatus = "Open"
        if activity?.activityDate == nil || activity?.customerName == nil || activity?.activityTime == nil || activity?.activityType == nil || activity?.activityDescription == nil {
            self.showAlert(title: "Warning !", message: "Enter all the fields")
            return
        }
        appdelegte.saveContext()
        activity = nil
         self.navigationController?.popViewController(animated: true)
    }
    func showAlert(title:String,message:String){
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func fetchCustomerNames()->[String]{
        
        var namesArray = [String]()
        
        for dic in appdelegte.customerList! {
            
            namesArray.append(dic["name"] as! String)
            
        }
        
        return namesArray
        
        
    }
    
    @IBAction func done(){
        activity?.activityDate = Date.dateToString(date: datePicker.date)
        activity?.activityTime = Date.dateToTimeString(date:datePicker.date)
        currentTextField?.resignFirstResponder()
        activityTableView.reloadData()
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}

extension CreateNewActivityController : UITableViewDelegate {
    
    
    
}

extension CreateNewActivityController : UITableViewDataSource {
    
    public func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return titlesArray.count
    }
    
    
    public func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "NewActivityCellTableViewCell") as! NewActivityCellTableViewCell
        let dic = titlesArray[indexPath.row]
        cell.label.text = dic["Title"]
        cell.textField.tag = indexPath.row
        switch indexPath.row {
        case 0:
            cell.textField.inputView = dateView
            currentTextField = cell.textField
            cell.textField.text = "\(activity?.activityDate ?? "") \(activity?.activityTime ?? "")"
        case 1:
            cell.textField.isUserInteractionEnabled = false
            cell.textField.text = "\(activity?.customerName ?? "")"
        case 2:
            cell.textField.text = "\(activity?.activityType ?? "")"
        case 3:
            cell.textField.text = "\(activity?.activityDescription ?? "")"
        case 4:
            cell.textField.text = "\(activity?.customerAddress ?? "")"
        case 5:
            cell.textField.keyboardType = .phonePad
            cell.textField.text = "\(activity?.customerPhone ?? "")"
        case 6:
            cell.textField.keyboardType = .numberPad
            cell.textField.text = "\(activity?.customerOutcome ?? "")"
        case 7:
            cell.textField.keyboardType = .emailAddress
            cell.textField.text = "\(activity?.customerEmail ?? "")"
        default: break
            
        }
        return cell
        
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath.row == 1{
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let dropdownView = storyboard.instantiateViewController(withIdentifier: "DropDownViewController") as! DropDownViewController
            dropdownView.delegate = self
            dropdownView.tableArray = self.fetchCustomerNames()
            isDropDownVisible = true
            self.navigationController?.pushViewController(dropdownView, animated: true)
        }
    }
    
    
    public func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
}

extension CreateNewActivityController : UITextFieldDelegate {
    
    func textFieldDidEndEditing(_ textField: UITextField){
        switch textField.tag {
        case 2: activity?.activityType = textField.text
        case 3: activity?.activityDescription = textField.text
        case 4: activity?.customerAddress = textField.text
        case 5: activity?.customerPhone = textField.text
        case 6: activity?.customerOutcome = textField.text
        case 7: activity?.customerEmail = textField.text
        default: break
            
        }
        
    }
    
    
}
extension CreateNewActivityController:DropDownDelegate{
    func didSelectDropDown(selectedObject: String, selectedIndex: Int) {
        activity?.customerName = selectedObject
        activity?.activityId = appdelegte.customerList?[selectedIndex]["custId"] as? String
        activityTableView.reloadData()
        isDropDownVisible = false
    }
}
