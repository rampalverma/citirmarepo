//
//  UIView+Extension.swift
//  VAgent
//
//  Created Madhav Bhogapurpau on 15/03/17.
//  Copyright © 2017 Admin. All rights reserved.
//

import Foundation
import UIKit
extension UIView {
    
//    public class func fromNib() -> Self {
//        return fromNib(nil)
//    }
    
    public class func fromNib(_ nibName: String?, tag : Int) -> Self {
        
        func fromNibHelper<T >(_ nibName: String?) -> T where T : UIView {
            let bundle = Bundle(for: T.self)
            let className = nibName ?? String(describing: T.self)
            var requiredView : T!
            let viewsArray =  bundle.loadNibNamed(className, owner: self, options: nil) as! [T]
                for view in viewsArray
            {
                if view.tag == tag
                {
                    requiredView = view
                    break
                }
            }
            
            return requiredView
        }
        
        return fromNibHelper(nibName)
    }
}

extension UIView {
    
    func addTapGesture(tapNumber : Int, target: Any , action : Selector) {
        
        let tap = UITapGestureRecognizer(target: target, action: action)
        tap.numberOfTapsRequired = tapNumber
        addGestureRecognizer(tap)
        isUserInteractionEnabled = true
        
    }
}
