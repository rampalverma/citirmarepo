//
//  UIImageView+Extension.swift
//  CITIRMApp
//
//  Created by Madhav Bhogapurapu on 19/07/17.
//  Copyright © 2017 Rampalverma. All rights reserved.
//

import UIKit

extension UIImageView
{
    func drawCircle()
    {
        self.layer.borderWidth = 1
        self.layer.masksToBounds = false
        self.layer.cornerRadius = self.frame.height/2
        self.clipsToBounds = true
    }
}
