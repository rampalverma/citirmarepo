//
//  UIViewController+Extension.swift
//  VAgent
//
//  Created by Sudheer Golla on 01/03/17.
//  Copyright © 2017 Admin. All rights reserved.
//

import UIKit

public extension UIViewController {
   
    @objc
     public func showAlert(_ title: String?, message:String?, buttonTitles:[String]?, handler:@escaping ((_ action: UIAlertAction) ->Void) )
    {
       let alertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
        if let otherTitles = buttonTitles {
            for (_, actionTitle) in (otherTitles.enumerated()) {
                let action = UIAlertAction(title: actionTitle, style: .default, handler: handler)
                alertController.addAction(action)
            }
        }
    
        self.present(alertController, animated: true, completion: nil)
    }
    
    //@objc
    @objc
    
    public func stringClassFromString(_ className: String) -> AnyClass! {
        
        /// get namespace
        let namespace = Bundle.main.infoDictionary!["CFBundleExecutable"] as! String;
        
        /// get 'anyClass' with classname and namespace
        let cls: AnyClass = NSClassFromString("\(namespace).\(className)")!;
        
        // return AnyClass!
        return cls;
    }
   
}

 class UIViewController_Extension: UIViewController {

    

}
