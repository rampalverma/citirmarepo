//
//  Date+Extension.swift
//  VAgent
//
//  Created by Madhav bhogapurapu on 21/04/17.
//  Copyright © 2017 Admin. All rights reserved.
//


import Foundation


extension Date
{
     static func dateToString(date: Date) ->String
    {
        let dateFormatter = DateFormatter()
        dateFormatter.timeZone = TimeZone.current //or use UTD or GMT
        dateFormatter.dateFormat = "dd/MM/yyyy"
        return dateFormatter.string(from: date)
        
    }
    static func dateAndTimeToString(date: Date) -> String
    {
        let formatter = DateFormatter()
        formatter.dateFormat = "dd/MM/YYYY hh:mm a"
        return formatter.string(from: date)
    }

    static func dateToString(date: Date, withFormat  format: String) ->String{
        let dateFormatter = DateFormatter()
        dateFormatter.timeZone = TimeZone.current //or use UTC or GMT
        dateFormatter.dateFormat = format
        return dateFormatter.string(from: date)
    }

    static func stringToDate(stringDate: String, withFormat format: String) ->Date
    {
        let dateFormatter = DateFormatter()
        dateFormatter.timeZone = TimeZone.current //or use UTC or GMT
        dateFormatter.dateFormat = format
        return dateFormatter.date(from: stringDate)!
    }
    static func dateToTimeString(date: Date?, stringDate dateString: String?) ->String
    {
        var sourceDate = date
        let dateFormatter = DateFormatter()
        dateFormatter.timeZone = TimeZone.current //or use UTC or GMT
        dateFormatter.dateFormat = "hh:mm"
        
        if sourceDate == nil && dateString != nil
        {
           sourceDate = Date.stringToDate(stringDate: dateString!, withFormat: "dd/MM/yyyy hh:mm")
        }
        return dateFormatter.string(from: sourceDate!)

    }

    static func dateToTimeString(date: Date?) ->String
    {
        let sourceDate = date
        let dateFormatter = DateFormatter()
        dateFormatter.timeZone = TimeZone.current //or use UTC or GMT
        dateFormatter.dateFormat = "hh:mm"
        return dateFormatter.string(from: sourceDate!)
        
    }

}
