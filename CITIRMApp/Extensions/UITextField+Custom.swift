//
//  UITextField+Custom.swift
//  CITIRMApp
//
//  Created by Madhav Bhogapurapu on 18/07/17.
//  Copyright © 2017 Rampalverma. All rights reserved.
//

import UIKit

@IBDesignable class CustomTextField: UITextField {
    @IBInspectable var left: CGFloat = 0
    @IBInspectable var right: CGFloat = 0
    @IBInspectable var top: CGFloat = 0
    @IBInspectable var bottom: CGFloat = 0
    //@IBInspectable var borderColor: CGColor = UIColor.gray.cgColor
    @IBInspectable  var padding = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 5);
    
    // placeholder position
    override func textRect(forBounds bounds: CGRect) -> CGRect {
        padding = UIEdgeInsets(top: top, left: left, bottom: bottom, right: right);
        
        return UIEdgeInsetsInsetRect(bounds, padding)
    }
    
    // text position
    override func editingRect(forBounds bounds: CGRect) -> CGRect {
        padding = UIEdgeInsets(top: top, left: left, bottom: bottom, right: right);
        
        return UIEdgeInsetsInsetRect(bounds, padding)
        //bounds.insetBy(dx: left, dy: right)
    }
    
    @IBInspectable var borderColor: UIColor? {
        didSet{
            self.layer.borderColor = borderColor?.cgColor
        }
    }
    @IBInspectable var borderWidth: CGFloat = 0 {
        didSet{
            self.layer.borderWidth = borderWidth
        }
    }
    @IBInspectable var cornerRadius: CGFloat = 1 {
        didSet{
            self.layer.cornerRadius = cornerRadius
        }
    }
}
