//
//  UIFont.swift
//  CitiControl
//
//  Created by Madhav Bhogapurapu on 12/06/17.
//  Copyright © 2017 VirtusaPolaris. All rights reserved.
//

import Foundation
import UIKit

//Helvetica-Regular
//Helvetica-Bold
//Helvetica-BoldOblique
//Helvetica-Light
//Helvetica-LightOblique
//Helvetica-Oblique
extension Float {
    var convertAsLocaleCurrency :String {
        let formatter = NumberFormatter()
        formatter.numberStyle = .currency
        formatter.locale = Locale.current
        return formatter.string(from: self as NSNumber)!
    }
}
extension NSNumber
{
     class func formattedCurrency(price:Double)->String{
        let convertPrice = NSNumber(value: price)
        
        let formatter = NumberFormatter()
        formatter.numberStyle = .currency
        //formatter.currencyCode = "INR"
        formatter.locale = Locale.current
        let convertedPrice = formatter.string(from: convertPrice)
        return convertedPrice!
    }
}


extension UIFont
{
    
    class func arialRegular(size: CGFloat) -> UIFont {
        return UIFont(name: "Arial", size: size)!
    }

    class func arialItalic(size: CGFloat) -> UIFont {
        return UIFont(name: "Arial-Italic", size: size)!
    }

    class func arialBold(size: CGFloat) -> UIFont {
        return UIFont(name: "Arial-Bold", size: size)!
    }
    
    
    class func helveticaRegular(size: CGFloat) -> UIFont {
        return UIFont(name: "Helvetica-Regular", size: size)!
    }

    class func helveticaItalic(size: CGFloat) -> UIFont {
        return UIFont(name: "Helvetica-Italic", size: size)!
    }

    class func boldSystemFontOfSize(size: CGFloat) -> UIFont {
        return UIFont(name: "Helvetica-Bold", size: size)!
    }

    class func mediumSystemFontOfSize(size: CGFloat) -> UIFont {
        return UIFont(name: "HelveticaNeue-Medium", size: size)!
    }

    class func helveticaLight(size: CGFloat) -> UIFont {
        return UIFont(name: "Helvetica-Light", size: size)!
    }

    class func thinSystemFontOfSize(size: CGFloat) -> UIFont {
        return UIFont(name: "HelveticaNeue-Thin", size: size)!
    }

    class func ultraLightSystemFontOfSize(size: CGFloat) -> UIFont {
        return UIFont(name: "HelveticaNeue-UltraLight", size: size)!
    }

}
