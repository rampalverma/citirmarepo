//
//  ProgressView.swift
//  CITIRMApp
//
//  Created by Madhav Bhogapurapu on 18/07/17.
//  Copyright © 2017 Rampalverma. All rights reserved.
//

import UIKit

class ProgressView: UIView {

    var porgressBGColor : UIColor?
    var titleText : String?
    
    @IBInspectable var bgColor : UIColor = UIColor.gray{
        didSet{
            self.porgressBGColor = bgColor
        }

    }
    
    
    @IBInspectable var text : String = ""{
        didSet{
            self.titleText = text
        }
        
    }
    
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
        //self.backgroundColor = UIColor.clear
        if self.porgressBGColor != nil
        {
            self.drawProgressBar(rect: rect, color: self.porgressBGColor!)
        }

    }
    
//    override init(frame : CGRect)
//    {
//        super.init(frame: frame)
//        
//    }
    
     override convenience init(frame : CGRect)
    {
        self.init(frame: frame)
        self.drawProgressBar(rect: self.frame, color: self.porgressBGColor!)

    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        var rect = self.frame
        rect.size.height = 10
        
        self.draw(rect)
//        if self.porgressBGColor != nil
//        {
//            self.drawProgressBar(rect: self.frame, color: self.porgressBGColor!)
//        }
        
    }
    
    func drawProgressBar(rect :CGRect, color : UIColor)
    {
        guard let context = UIGraphicsGetCurrentContext() else { return }
        
        context.beginPath()
        context.move(to: CGPoint(x: rect.minX, y: rect.maxY))
        context.addLine(to: CGPoint(x: rect.minX + 10, y: rect.minX))
        context.addLine(to: CGPoint(x: rect.maxX, y: rect.minY))
        context.addLine(to: CGPoint(x: rect.maxX, y: rect.maxY))

        context.addLine(to: CGPoint(x: (rect.minX), y: rect.maxY))
        context.closePath()
        context.setFillColor(color.cgColor)
        context.fillPath()
        if self.titleText != nil
        {
        self.drawMyText(myText: self.titleText!, textColor: .black, font: UIFont.arialRegular(size: 12), inRect: rect)
        }
        
        
        
    }
    func drawMyText(myText:String,textColor:UIColor, font: UIFont, inRect:CGRect){
        var rect = inRect
        rect.origin.x += 15
        let textFontAttributes = [
            NSFontAttributeName: font,
            NSForegroundColorAttributeName: textColor,
            ] as [String : Any]
        
        myText.draw(in: rect, withAttributes: textFontAttributes)
    }
    
    override func updateConstraints() {
        //set subview constraints here
        super.updateConstraints()
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        //manually set subview frames here
    }
    
    override func setNeedsDisplay() {
        super.setNeedsDisplay()
        
        
    }
}
