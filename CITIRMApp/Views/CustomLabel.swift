//
//  CustomLabel.swift
//  CITIRMApp
//
//  Created by rampal verma on 22/07/17.
//  Copyright © 2017 Rampalverma. All rights reserved.
//

import UIKit


@IBDesignable class CustomLabel: UILabel {

    @IBInspectable var topInset: CGFloat = 0.0
    @IBInspectable var bottomInset: CGFloat = 0.0
    @IBInspectable var leftInset: CGFloat = 0.0
    @IBInspectable var rightInset: CGFloat = 0.0
    @IBInspectable var cornderRadius: CGFloat = 0.0
    @IBInspectable var bgColor: UIColor? = UIColor.clear
    override func awakeFromNib() {
        super.awakeFromNib()
        self.cornderRadius = 8
        self.layer.masksToBounds = true

    }

    
    override var intrinsicContentSize: CGSize {
        get {
            var contentSize = super.intrinsicContentSize
            contentSize.height += topInset + bottomInset
            contentSize.width += leftInset + rightInset
            return contentSize
        }
    }
    
    override func textRect(forBounds bounds: CGRect, limitedToNumberOfLines numberOfLines: Int) -> CGRect {
        return self.bounds.insetBy(dx: CGFloat(15.0), dy: CGFloat(15.0))
    }
    
    override func draw(_ rect: CGRect) {
        let insets = UIEdgeInsets(top: topInset, left: leftInset, bottom: bottomInset, right: rightInset)
        
        super.drawText(in: UIEdgeInsetsInsetRect(rect, insets))
    }
    
   
    
//    @IBInspectable var bgColor : UIColor = {
//        didSet {
//            self.back
//        }
//    }
//

}
