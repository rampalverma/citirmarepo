//
//  CustomTableView.swift
//  CITIRMApp
//
//  Created by Harshita on 14/07/17.
//  Copyright © 2017 Rampalverma. All rights reserved.
//

import Foundation
import UIKit

protocol CustomtTableViewDelegate: class {
    
    func didSelectRow(indexPath: IndexPath, table : CustomTableView)
}

extension CustomtTableViewDelegate
{
    func didSelectRow(indexPath: IndexPath, table : CustomTableView){
    }
}

class CustomTableView: UITableView, UITableViewDelegate, UITableViewDataSource, CustomtTableViewDelegate {
    
    var customDelegate : CustomtTableViewDelegate?
    var dataArray = [[String : Any]]()
    
    override init(frame: CGRect, style: UITableViewStyle) {
        super.init(frame: frame, style: style)
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        
    }
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    func getCellIdentifier() ->String
    {
        var identifier = "DashboardCellTableViewCell"
        if self.tag == 1
        {
           identifier = "DashboardCellTableViewCell"
        }
//        else if self.tag == 3
//        {
//         identifier = "ServiceRequestsCell"
//        }
        
        return identifier
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int{
        
            return dataArray.count
        
        //return 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell{
        let cellIdentifier = getCellIdentifier()
        
        let cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier, for: indexPath) as! DashboardCellTableViewCell
        
        
        setupCell(cell: cell, indexPath: indexPath)
        
        
        return cell
    }
    
    func setupCell(cell : DashboardCellTableViewCell, indexPath: IndexPath)
    {
        let dict = dataArray[indexPath.row]
        
       // switch self.tag {
        //case 1:
        cell.nameLabel.text = dict["name"] as? String
            cell.acntNumberLabel.text = dict["accountNumber"] as? String
            cell.acccountTypeLabel.text = dict["accountType"] as? String
            cell.descLabel.text = dict["description"] as? String
            cell.timeLabel.text = dict["dueTime"] as? String
            cell.makeRoundedLabel(text:cell.nameLabel.text)
            
//        case 2: cell.nameLabel.text = dict["name"] as? String
//        cell.acntNumberLabel.text = dict["accountNumber"] as? String
//        if let amount = dict["amount"] as? Double
//        {
//            cell.amountLabel.text = NSNumber.formattedCurrency(price: amount)
//        }
//        //cell.descLabel.text = dict["description"] as? String
//        
//        if let dateStr = dict["dueTime"] as? String
//        {
//            let shortDate = Date.dateToString(date: Date.stringToDate(stringDate: dateStr, withFormat: "MMM dd yyyy"), withFormat: "MMM dd")
//            cell.timeLabel.text = shortDate
//
//        }
//
//        case 3:
//        
//        cell.nameLabel.text = dict["name"] as? String
//        cell.acntNumberLabel.text = dict["accountNumber"] as? String
//        cell.acccountTypeLabel.text = dict["accountType"] as? String
//        cell.timeLabel.text = dict["dateTime"] as? String
//        cell.requestNumberLabel.text = dict["requestId"] as? String
////        if self.tag == 2
////        {
//        cell.progressView.titleText = dict["status"] as? String
//            cell.progressView.setNeedsDisplay()
//          //  }
//        default:
//            break
//        }
    }
    
    func reloadTableView(contentArray : [[String :Any]]?)
    {
//        for index in 0..<5
//        {
//            dataArray.append(["Title" : "Value \(index)"])
//            
//        }
        if let _ = contentArray
        {
            dataArray = contentArray!
        if self.tag == 3
        {
            dataArray.removeFirst()
        }
            
        if self.dataSource == nil
        {
            self.dataSource = self
            self.delegate = self
        }
            
//        else
//        {
//            self.reloadData()
//        }
        }
        
//        DispatchQueue.main.async {
//            for index in 1..<4
//            self.tag = index
//
//            {self.reloadData()
//    
//        }

//        if self.dataArray?.count == 0
//        {
//            self.dataArray = contentArray as? [[String :Any]]
//        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        didSelectRow(indexPath: indexPath)
        
    }
    
    func didSelectRow(indexPath: IndexPath) {
        
        if let _ = customDelegate
        {
            customDelegate?.didSelectRow(indexPath: indexPath, table : self)
        }
        
    }

}
