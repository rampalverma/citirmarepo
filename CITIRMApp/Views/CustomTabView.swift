//
//  CustomTabView.swift
//  CitiControl
//
//  Created by Madhav Bhogapurapu on 14/07/17.
//  Copyright © 2017 VirtusaPolaris. All rights reserved.
//

import UIKit

@objc protocol CustomTabDelegate : class {
 func didSelecTabButton(withIndex index: Int, item : UIButton)
    
}
extension CustomTabDelegate {
    func didSelecTabButton(withIndex index: Int, item : UIButton){}
}





@IBDesignable class CustomTabView: UIView {

    var buttons : Int!
    var padding : CGFloat?
    var titles : [String] = ["Event","Escalations","Action  Plans "]
    var defaultColor : UIColor = .clear
    var currentIndex : Int = 0
    var borderView = UILabel()

    var customDelegate : CustomTabDelegate?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.createTabButtons()
        
    }
    
    override func layoutSubviews() {
    super.layoutSubviews()
        
        if self.subviews.count == 0
            {
            self.createTabButtons()
          }
        else
        {
            for view in self.subviews
            {
                view.removeFromSuperview()
                self.createTabButtons()
            }
        }
    }
    
    convenience init(titles : [String])
    {
        self.init()
        self.titles = titles

    }
    
    override init(frame: CGRect)
    {
        super.init(frame: frame)
        self.createTabButtons()
 
        
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        self.createTabButtons()

    }
    
    @IBInspectable var numberOfButtons: Int = 0 {
        didSet {
            self.buttons = numberOfButtons
            self.createTabButtons()

        }
    }
    
    @IBInspectable var buttonTitlesString: String = "" {
        didSet {
            self.titles = buttonTitlesString.components(separatedBy: ",")
            self.createTabButtons()

        }
    }
    @IBInspectable var cornerRadius: CGFloat = 0 {
        didSet {
            layer.cornerRadius = cornerRadius
            layer.masksToBounds = cornerRadius > 0
        }
    }
    @IBInspectable var borderWidth: CGFloat = 0 {
        didSet {
            layer.borderWidth = borderWidth
        }
    }
    @IBInspectable var borderColor: UIColor? {
        didSet {
            layer.borderColor = borderColor?.cgColor
        }
    }
    
    @IBInspectable var buttonSpacing : CGFloat = 2 {
        didSet {
            self.padding = buttonSpacing
        }
    
    }
    
    @IBInspectable var tabItemDefaultColor : UIColor? {
        didSet {
            self.defaultColor = tabItemDefaultColor!
        }
        
    }

    func createTabButtons()
    {
        
        var rect = self.bounds
        if buttons != nil && self.padding != nil
        {
          
            for view in self.subviews
            {
                view.removeFromSuperview()
            }
        let btnWd = (rect.size.width - CGFloat(buttons) * self.padding!)/CGFloat(buttons)
        rect.size.width = btnWd
        
        for index in 0..<buttons
        {

            let tabItem = UIButton(type: .custom)
            
            tabItem.setTitleColor(UIColor.lightGray, for: .normal)
            tabItem.titleLabel?.font = UIFont(name: "Arial", size: 11)
            
            rect.origin.x = (btnWd + padding!) * CGFloat(index)
            tabItem.frame = rect
            tabItem.setTitle(titles[index], for: .normal)
            tabItem.addTarget(self, action: #selector(tabButtonAction(btn: )), for: .touchUpInside)
            tabItem.tag = index
            //tabItem.layer.borderWidth = 1
            addSubview(tabItem)
            
            var borderRect = rect
            borderView = UILabel()
            borderRect.origin.y = rect.size.height - 2
            borderRect.size.height = 2
            borderView.frame = borderRect
            borderView.tag = index
            borderView.isHidden = false

            addSubview(borderView)
            borderView.backgroundColor = self.defaultColor

            if index == 0
            {
                borderView.isHidden = false
                borderView.backgroundColor = lightBlueColor
            }
        }
        }
        
    }
    
    func highlightTabItem(btn: UIButton)
    {
        for (_,borderView) in (self.subviews.filter{$0 .isKind(of: UILabel.self)}).enumerated(){
            
      //  borderView.isHidden = (borderView.tag == btn.tag) ? false:true
        borderView.backgroundColor = (borderView.tag == btn.tag) ? lightBlueColor : .clear
        }
    }
    func tabButtonAction(btn: UIButton)
    {
        var type = kCATransitionFromLeft
        
        if self.currentIndex>btn.tag
        {
            type = kCATransitionFromRight
        }
        
        let transition: CATransition = CATransition()
        transition.duration = 0.2
        transition.type = kCATransitionReveal
        transition.subtype = type
        self.borderView.layer.add(transition, forKey: nil)
        
        self.currentIndex = btn.tag
        highlightTabItem(btn: btn)
        self.customDelegate?.didSelecTabButton(withIndex: btn.tag, item: btn)
    }
    
    
}
