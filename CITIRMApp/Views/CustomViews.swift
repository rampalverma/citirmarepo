//
//  CustomViews.swift
//  CITIRMApp
//
//  Created by Madhav Bhogapurapu on 17/07/17.
//  Copyright © 2017 Rampalverma. All rights reserved.
//

import UIKit

class CustomViews: UIView {
    @IBOutlet weak var borderView: UIButton!

    @IBOutlet weak var tileImageView: UIImageView!
    @IBOutlet weak var titleLabel: UILabel!
    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */

}
