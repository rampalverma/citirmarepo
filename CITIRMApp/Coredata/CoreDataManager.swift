//
//  CoreDataManager.swift
//  VAgent
//
//  Created by Madhav Bhogapurapu on 03/04/17.
//  Copyright © 2017 Admin. All rights reserved.
//

import UIKit
import CoreData

class CoreDataManager: NSObject {
static let shared = CoreDataManager()
private let appDel = UIApplication.shared.delegate as! AppDelegate

    func getManagedContext()->NSManagedObjectContext?
    {
        if #available(iOS 10.0, *) {
            return appDel.persistentContainer.viewContext
        } else {
            // Fallback on earlier versions
        }
        return nil
    }
    
    func saveCoreDataContext(){
        appDel.saveContext()
    }
    
    func insertData(intoEntity: String?, model: AnyObject?)
    {
        
    }
    
    func getUserDetailsWith(id:String)  {
        
    }
    
//    func saveUser(user:UserDetails)  {
//        
//    }
    
    func isEntityEmpty(entity : String) ->  Bool {
        do{
            let request = NSFetchRequest<NSFetchRequestResult>(entityName: entity)
            let count  = try getManagedContext()?.count(for: request)
            return count == 0 ? true : false
        }catch{
            return true
        }
    }
}
