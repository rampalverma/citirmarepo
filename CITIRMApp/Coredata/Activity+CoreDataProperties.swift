//
//  Activity+CoreDataProperties.swift
//  CITIRMApp
//
//  Created by admin on 14/07/17.
//  Copyright © 2017 Rampalverma. All rights reserved.
//

import Foundation
import CoreData


extension Activity {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<Activity> {
        return NSFetchRequest<Activity>(entityName: "Activity")
    }

    @NSManaged public var activityDate: String?
    @NSManaged public var activityTime: String?
    @NSManaged public var activityName: String?
    @NSManaged public var activityId: String?
    @NSManaged public var members: String?
    @NSManaged public var createdBy: String?
    @NSManaged public var activityType: String?
    @NSManaged public var activityDescription: String?
    @NSManaged public var activityStatus: String?
    @NSManaged public var customerAddress: String?
    @NSManaged public var customerPhone: String?
    @NSManaged public var customerOutcome: String?
    @NSManaged public var customerEmail: String?
    @NSManaged public var customerName: String?

}
